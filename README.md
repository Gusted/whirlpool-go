# Whirlpool hash function

Whirlpool hash function in idiomatic Go using interfaces by the standard library.
Reference:
- [The WHIRLPOOL Hash Function](https://web.archive.org/web/20171129084214/http://www.larc.usp.br/~pbarreto/WhirlpoolPage.html).

Don't expect too much of this. There's **no guarentee** that this is actually safe, use at your own risk. It might delete your files if the cat in the void meows.

## Examples

Checksum of a file:
``` go
f, err := os.Open("README.md")
if err != nil {
    // [...]
}
defer f.Close()

h := whirlpool.New()
io.Copy(h, f)
fileHash := h.Sum(nil)
```

Checksum of a message:
``` go
message := []byte("Hello, world")
messageDigest := whirlpool.Sum(message)
```

## Benchmarks

AMD Ryzen 5 3600X Linux:
<details>

```
                           │   sec/op    │
Whirlpool/WriteSum-8-12      469.2n ± 4%
Whirlpool/WriteSum-16-12     460.1n ± 2%
Whirlpool/WriteSum-32-12     914.2n ± 3%
Whirlpool/WriteSum-64-12     923.6n ± 3%
Whirlpool/WriteSum-128-12    1.394µ ± 5%
Whirlpool/WriteSum-256-12    2.264µ ± 4%
Whirlpool/WriteSum-512-12    4.077µ ± 3%
Whirlpool/WriteSum-1024-12   7.681µ ± 2%
Whirlpool/WriteSum-2048-12   14.69µ ± 2%
Whirlpool/WriteSum-4096-12   29.53µ ± 4%
geomean                      2.451µ

                           │     B/s      │
Whirlpool/WriteSum-8-12      16.26Mi ± 3%
Whirlpool/WriteSum-16-12     33.17Mi ± 2%
Whirlpool/WriteSum-32-12     33.38Mi ± 3%
Whirlpool/WriteSum-64-12     66.08Mi ± 3%
Whirlpool/WriteSum-128-12    87.58Mi ± 5%
Whirlpool/WriteSum-256-12    107.9Mi ± 4%
Whirlpool/WriteSum-512-12    119.8Mi ± 3%
Whirlpool/WriteSum-1024-12   127.1Mi ± 2%
Whirlpool/WriteSum-2048-12   132.9Mi ± 2%
Whirlpool/WriteSum-4096-12   132.3Mi ± 4%
geomean                      70.45Mi
```

</details>

Raspberry Pi 4B Linux:
<details>

```
                          │   sec/op    │
Whirlpool/WriteSum-8-4      2.435µ ± 1%
Whirlpool/WriteSum-16-4     2.359µ ± 2%
Whirlpool/WriteSum-32-4     4.705µ ± 0%
Whirlpool/WriteSum-64-4     4.635µ ± 1%
Whirlpool/WriteSum-128-4    6.747µ ± 1%
Whirlpool/WriteSum-256-4    11.12µ ± 1%
Whirlpool/WriteSum-512-4    19.67µ ± 1%
Whirlpool/WriteSum-1024-4   36.73µ ± 2%
Whirlpool/WriteSum-2048-4   71.02µ ± 1%
Whirlpool/WriteSum-4096-4   139.5µ ± 2%

                          │     B/s      │
Whirlpool/WriteSum-8-4      3.138Mi ± 1%
Whirlpool/WriteSum-16-4     6.466Mi ± 2%
Whirlpool/WriteSum-32-4     6.485Mi ± 0%
Whirlpool/WriteSum-64-4     13.17Mi ± 1%
Whirlpool/WriteSum-128-4    18.10Mi ± 1%
Whirlpool/WriteSum-256-4    21.96Mi ± 1%
Whirlpool/WriteSum-512-4    24.82Mi ± 1%
Whirlpool/WriteSum-1024-4   26.59Mi ± 2%
Whirlpool/WriteSum-2048-4   27.50Mi ± 1%
Whirlpool/WriteSum-4096-4   28.00Mi ± 2%
```

</details>

## License

The code is licensed under [the MIT License](./LICENSE).

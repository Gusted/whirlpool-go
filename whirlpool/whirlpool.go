// Package whirlpool implements the Whirlpool hash function.
package whirlpool

import (
	"encoding/binary"
	"hash"
)

const (
	// The blocksize of Whirlpool in bytes.
	BlockSize = 64

	// The hash size of Whirlpool in bytes.
	Size = 64

	// The digest size of Whirlpool in bytes.
	digestSize = BlockSize / 2
)

type whirlpoolState struct {
	// 512-bit internal secret
	s [8]uint64
	// Buffer for yet to be processed data.
	buf [BlockSize]byte
	// Number of bytes in the buffer.
	bufN int
	// The amount of bits written, maximum of 2²⁵⁶.
	bitsWritten [digestSize]byte
}

// New returns a new [hash.Hash] computing the Whirlpool checksum.
func New() hash.Hash {
	return &whirlpoolState{}
}

// Sum returns the Whirlpool checksum of the data.
func Sum(data []byte) [Size]byte {
	var sum [Size]byte
	h := whirlpoolState{}
	h.Write(data)
	h.finalize()

	// WHIRLPOOL(M) = μ⁻¹(Hₜ).
	for i := 0; i < 64; i += 8 {
		binary.BigEndian.PutUint64(sum[i:i+8], h.s[i>>3])
	}
	return sum
}

func (w *whirlpoolState) Reset() {
	w.s = [8]uint64{}
	w.buf = [BlockSize]byte{}
	w.bufN = 0
	w.bitsWritten = [digestSize]byte{}
}

func (*whirlpoolState) BlockSize() int { return BlockSize }
func (*whirlpoolState) Size() int      { return Size }

func (w *whirlpoolState) Write(p []byte) (n int, err error) {
	n = len(p)

	// Tally the length of the data added.
	for i, carry, value := 31, uint16(0), uint64(n*8); i >= 0 && (carry != 0 || value != 0); i-- {
		carry += uint16(w.bitsWritten[i]) + (uint16(value & 0xff))
		w.bitsWritten[i] = byte(carry)
		carry >>= 8
		value >>= 8
	}

	// First try to empty the buffer.
	if w.bufN > 0 {
		rem := copy(w.buf[w.bufN:], p)
		w.bufN += rem
		if w.bufN == BlockSize {
			w.process()
			w.bufN = 0
		}
		p = p[rem:]
	}

	// Simply process the message if it's larger than the blocksize.
	for len(p) >= BlockSize {
		copy(w.buf[:], p)
		w.process()
		p = p[BlockSize:]
	}

	// Flush the rest to the buffer.
	if len(p) > 0 {
		w.bufN = copy(w.buf[:], p)
	}

	return
}

func (w *whirlpoolState) process() {
	L := [8]uint64{}
	// μ buffer.
	buf := [8]uint64{}
	// Cipher state.
	state := [8]uint64{}
	// Round key,
	K := w.s

	// Map incoming data to an buffer.
	// Compute and apply K⁰ to the cipher state.
	for i := 0; i < 8; i++ {
		buf[i] = binary.BigEndian.Uint64(w.buf[i*8:])
		state[i] = buf[i] ^ K[i]
	}

	// Round function.
	for i := 0; i < 10; i++ {
		// Compute Kʳ from Kʳ⁻¹.
		for j := 0; j < 8; j += 4 {
			k0, k1, k2, k3, k4, k5, k6, k7 := K[j], K[(j+7)&7], K[(j+6)&7], K[(j+5)&7], K[(j+4)&7], K[j+3], K[j+2], K[j+1]
			L[j] = c0[byte(k0>>56)] ^
				c1[byte(k1>>48)] ^
				c2[byte(k2>>40)] ^
				c3[byte(k3>>32)] ^
				c4[byte(k4>>24)] ^
				c5[byte(k5>>16)] ^
				c6[byte(k6>>8)] ^
				c7[byte(k7)]

			L[j+1] = c0[byte(k7>>56)] ^
				c1[byte(k0>>48)] ^
				c2[byte(k1>>40)] ^
				c3[byte(k2>>32)] ^
				c4[byte(k3>>24)] ^
				c5[byte(k4>>16)] ^
				c6[byte(k5>>8)] ^
				c7[byte(k6)]

			L[j+2] = c0[byte(k6>>56)] ^
				c1[byte(k7>>48)] ^
				c2[byte(k0>>40)] ^
				c3[byte(k1>>32)] ^
				c4[byte(k2>>24)] ^
				c5[byte(k3>>16)] ^
				c6[byte(k4>>8)] ^
				c7[byte(k5)]

			L[j+3] = c0[byte(k5>>56)] ^
				c1[byte(k6>>48)] ^
				c2[byte(k7>>40)] ^
				c3[byte(k0>>32)] ^
				c4[byte(k1>>24)] ^
				c5[byte(k2>>16)] ^
				c6[byte(k3>>8)] ^
				c7[byte(k4)]
		}

		// Introduce round constant.
		L[0] ^= rc[i]
		copy(K[:], L[:])

		// Apply the r-th round transformation.
		for j := 0; j < 8; j += 4 {
			s0, s1, s2, s3, s4, s5, s6, s7 := state[j], state[(j+7)&7], state[(j+6)&7], state[(j+5)&7], state[(j+4)&7], state[j+3], state[j+2], state[j+1]
			L[j] ^= c0[byte(s0>>56)] ^
				c1[byte(s1>>48)] ^
				c2[byte(s2>>40)] ^
				c3[byte(s3>>32)] ^
				c4[byte(s4>>24)] ^
				c5[byte(s5>>16)] ^
				c6[byte(s6>>8)] ^
				c7[byte(s7)]

			L[j+1] ^= c0[byte(s7>>56)] ^
				c1[byte(s0>>48)] ^
				c2[byte(s1>>40)] ^
				c3[byte(s2>>32)] ^
				c4[byte(s3>>24)] ^
				c5[byte(s4>>16)] ^
				c6[byte(s5>>8)] ^
				c7[byte(s6)]

			L[j+2] ^= c0[byte(s6>>56)] ^
				c1[byte(s7>>48)] ^
				c2[byte(s0>>40)] ^
				c3[byte(s1>>32)] ^
				c4[byte(s2>>24)] ^
				c5[byte(s3>>16)] ^
				c6[byte(s4>>8)] ^
				c7[byte(s5)]

			L[j+3] ^= c0[byte(s5>>56)] ^
				c1[byte(s6>>48)] ^
				c2[byte(s7>>40)] ^
				c3[byte(s0>>32)] ^
				c4[byte(s1>>24)] ^
				c5[byte(s2>>16)] ^
				c6[byte(s3>>8)] ^
				c7[byte(s4)]
		}

		copy(state[:], L[:])
	}

	// Apply the Miyaguchi-Preneel compression function.
	//
	// ηᵢ = μ(mᵢ)
	// Hᵢ = W[Hᵢ₋₁](ηᵢ) ⨁ Hᵢ₋₁ ⨁ ηᵢ
	for i := 0; i < 8; i++ {
		w.s[i] ^= state[i] ^ buf[i]
	}
}

var zeroes = make([]byte, 32)

func (w *whirlpoolState) finalize() {
	// 10* Padding.
	w.buf[w.bufN] = 0x80
	w.bufN++

	if w.bufN > BlockSize-digestSize {
		if w.bufN < BlockSize {
			copy(w.buf[w.bufN:], zeroes)
		}

		w.process()
		w.bufN = 0
	}

	if w.bufN < digestSize {
		copy(w.buf[w.bufN:digestSize], zeroes)
	}

	// Append the bit length of the hashed data,
	// in the last 256 bits of the buffer.
	copy(w.buf[digestSize:], w.bitsWritten[:])

	// Process this data block.
	w.process()
}

func (w *whirlpoolState) Sum(sum []byte) []byte {
	w0 := *w
	w0.finalize()

	// WHIRLPOOL(M) = μ⁻¹(Hₜ).
	for i := 0; i < 8; i++ {
		sum = binary.BigEndian.AppendUint64(sum, w0.s[i])
	}

	return sum
}

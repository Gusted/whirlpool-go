package whirlpool

import (
	"bytes"
	"encoding/hex"
	"hash"
	"strconv"
	"strings"
	"testing"
)

func TestKAT(t *testing.T) {
	testCases := []struct {
		input    string
		expected string
	}{
		// ISO Test vectors.
		{
			input:    "",
			expected: "19FA61D75522A4669B44E39C1D2E1726C530232130D407F89AFEE0964997F7A73E83BE698B288FEBCF88E3E03C4F0757EA8964E59B63D93708B138CC42A66EB3",
		},
		{
			input:    "a",
			expected: "8ACA2602792AEC6F11A67206531FB7D7F0DFF59413145E6973C45001D0087B42D11BC645413AEFF63A42391A39145A591A92200D560195E53B478584FDAE231A",
		},
		{
			input:    "abc",
			expected: "4E2448A4C6F486BB16B6562C73B4020BF3043E3A731BCE721AE1B303D97E6D4C7181EEBDB6C57E277D0E34957114CBD6C797FC9D95D8B582D225292076D4EEF5",
		},
		{
			input:    "message digest",
			expected: "378C84A4126E2DC6E56DCC7458377AAC838D00032230F53CE1F5700C0FFB4D3B8421557659EF55C106B4B52AC5A4AAA692ED920052838F3362E86DBD37A8903E",
		},
		{
			input:    "abcdefghijklmnopqrstuvwxyz",
			expected: "F1D754662636FFE92C82EBB9212A484A8D38631EAD4238F5442EE13B8054E41B08BF2A9251C30B6A0B8AAE86177AB4A6F68F673E7207865D5D9819A3DBA4EB3B",
		},
		{
			input:    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",
			expected: "DC37E008CF9EE69BF11F00ED9ABA26901DD7C28CDEC066CC6AF42E40F82F3A1E08EBA26629129D8FB7CB57211B9281A65517CC879D7B962142C65F5A7AF01467",
		},
		{
			input:    "12345678901234567890123456789012345678901234567890123456789012345678901234567890",
			expected: "466EF18BABB0154D25B9D38A6414F5C08784372BCCB204D6549C4AFADB6014294D5BD8DF2A6C44E538CD047B2681A51A2C60481E88C5A20B2C2A80CF3A9A083B",
		},
		{
			input:    strings.Repeat("a", 1000000),
			expected: "0C99005BEB57EFF50A7CF005560DDF5D29057FD86B20BFD62DECA0F1CCEA4AF51FC15490EDDC47AF32BB2B66C34FF9AD8C6008AD677F77126953B226E4ED8B01",
		},
		// Nessie test vectors.
		{
			input:    "\x00",
			expected: "4D9444C212955963D425A410176FCCFB74161E6839692B4C11FDE2ED6EB559EFE0560C39A7B61D5A8BCABD6817A3135AF80F342A4942CCAAE745ABDDFB6AFED0",
		},
		{
			input:    "\x00\x00",
			expected: "8BDC9D4471D0DABD8812098B8CBDF5090BEDDB3D582917A61E176E3D22529D753FED9A37990CA18583855EFBC4F26E88F62002F67722EB05F74C7EA5E07013F5",
		},
		{
			input:    "\x00\x00\x00",
			expected: "86AABFD4A83C3551CC0A63185616ACB41CDFA96118F1FFB28376B41067EFA25FB6C889662435BFC11A4F0936BE6BCC2C3E905C4686DB06159C40E4DD67DD983F",
		},
		{
			input:    "\x00\x00\x00\x00",
			expected: "4ED6B52E915F09A803677C3131F7B34655D32817505D89A8CC07ED073CA3FEDDDD4A57CC53696027E824AB9822630087657C6FC6A28836CF1F252ED204BCA576",
		},
		{
			input:    strings.Repeat("\x00", 123),
			expected: "065F8A62B8EC8D3F3172015E32A5714C30627B53533DAAE508EC85FB90048DE708A2199D9EA4EFA6949139F7526CBD8BAB6402CBDA92DE82FAD802409CA2E772",
		},
		{
			input:    strings.Repeat("\x00", 127),
			expected: "CF73AB693E86E45FC33F9DC174443E7EA4E8ACB131257F5CEAC4503D9C7A1138342E2B80E6C4FDDB3B47B00C990283903039CB5622AC905B3B9C1ED7C9982194",
		},
		// Wikipedia
		{
			input:    "The quick brown fox jumps over the lazy dog",
			expected: "B97DE512E91E3828B40D2B0FDCE9CEB3C4A71F9BEA8D88E75C4FA854DF36725FD2B52EB6544EDCACD6F8BEDDFEA403CB55AE31F03AD62A5EF54E42EE82C3FB35",
		},
		{
			input:    "The quick brown fox jumps over the lazy eog",
			expected: "C27BA124205F72E6847F3E19834F925CC666D0974167AF915BB462420ED40CC50900D85A1F923219D832357750492D5C143011A76988344C2635E69D06F2D38C",
		},
	}

	for _, testCase := range testCases {
		actual := Sum([]byte(testCase.input))
		b, _ := hex.DecodeString(testCase.expected)
		if !bytes.Equal(actual[:], b) {
			t.Errorf("Testcase failed.\nWant: %x\nGot:  %x", b, actual)
		}
	}
}

func TestHashAPI(t *testing.T) {
	HashTest(t, New())
}

func HashTest(t *testing.T, hash hash.Hash) {
	t.Helper()

	t.Run("Reset", func(t *testing.T) {
		// Ensures that Reset really resets the internal state. This checks if
		// the buffer mechanism is correctly reset, by first 'buffering' a long
		// message, then resets and then writes a smaller message. The reset
		// should've caused that the buffer is cleaned and doesn't affect the
		// next buffered message.
		hash.Reset()
		longMessage := []byte{0x01, 0x02}
		shortMessage := []byte{0x01}

		hash.Write(shortMessage)
		sum1 := hash.Sum(nil)
		hash.Reset()

		hash.Write(longMessage)
		hash.Reset()

		hash.Write(shortMessage)
		sum2 := hash.Sum(nil)

		if !bytes.Equal(sum1, sum2) {
			t.Errorf("Sum 1: %x\nSum 2: %x", sum1, sum2)
		}
	})

	t.Run("Unalligned writes", func(t *testing.T) {
		// It shouldn't matter if a message is written unalligned or all at once.
		hash.Reset()
		buf := SequentialBytes(0x10000)

		hash.Write(buf)
		allignedWriteSum := hash.Sum(nil)
		if len(allignedWriteSum) != hash.Size() {
			t.Errorf("Sum isn't expected length of %d, actual %d", hash.Size(), len(allignedWriteSum))
		}
		hash.Reset()

		for i := 0; i < len(buf); {
			// Cycle through offsets which make a 137 byte sequence.
			// Because 137 is prime this sequence should exercise all corner cases.
			offsets := [17]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 1}
			for _, j := range offsets {
				// Don't read past the length of the buffer.
				if v := len(buf) - i; v < j {
					j = v
				}
				hash.Write(buf[i : i+j])
				i += j
			}
		}
		unallignedWriteSum := hash.Sum(nil)
		if !bytes.Equal(allignedWriteSum, unallignedWriteSum) {
			t.Errorf("Unalligned: %x\nAlligned:   %x", unallignedWriteSum, allignedWriteSum)
		}
	})

	t.Run("Appending sum", func(t *testing.T) {
		hash.Reset()
		hash.Write([]byte{0xcc})
		expected := hash.Sum(nil)
		hash.Reset()

		t.Run("Reallocation", func(t *testing.T) {
			for capacity := 2; capacity <= hash.Size()+2; capacity += hash.Size() {
				// The first time around the loop, Sum will have to reallocate.
				// The second time, it will not.
				buf := make([]byte, 2, capacity)
				hash.Write([]byte{0xcc})
				buf = hash.Sum(buf)
				hash.Reset()

				want := append([]byte{0x00, 0x00}, expected...)
				if !bytes.Equal(buf, want) {
					t.Errorf("Got:  %x\nWant: %x", buf, want)
				}
			}
		})

		t.Run("No reallocation", func(t *testing.T) {
			buf := make([]byte, 1, 200)
			hash.Write([]byte{0xcc})
			buf = hash.Sum(buf)
			hash.Reset()

			want := append([]byte{0x00}, expected...)
			if !bytes.Equal(buf, want) {
				t.Errorf("Got:  %x\nWant: %x", buf, want)
			}
		})
	})

	t.Run("Sum doesn't change state", func(t *testing.T) {
		message := SequentialBytes(hash.BlockSize())
		slice1, slice2 := message[:hash.BlockSize()/2], message[hash.BlockSize()/2:]

		hash.Write(message)
		tag1 := hash.Sum(nil)
		hash.Reset()

		hash.Write(slice1)
		_ = hash.Sum(nil)
		hash.Write(slice2)
		tag2 := hash.Sum(nil)
		hash.Reset()

		if !bytes.Equal(tag1, tag2) {
			t.Errorf("Tag 1: %x\nTag 2: %x", tag1, tag2)
		}
	})
}

// SequentialBytes produces a buffer of size consecutive bytes 0x00, 0x01, ...
func SequentialBytes(size int) []byte {
	result := make([]byte, size)
	for i := range result {
		result[i] = byte(i)
	}
	return result
}

var buf = make([]byte, 8192)

func benchmarWriteSum(b *testing.B, size int, bench hash.Hash) {
	sum := make([]byte, Size)

	b.ReportAllocs()
	b.SetBytes(int64(size))
	for i := 0; i < b.N; i++ {
		bench.Reset()
		bench.Write(buf[:size])
		bench.Sum(sum[:0])
	}
}

func BenchmarkWhirlpool(b *testing.B) {
	for _, size := range []int{8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096} {
		b.Run("WriteSum-"+strconv.Itoa(size), func(b *testing.B) {
			benchmarWriteSum(b, size, New())
		})
	}
}
